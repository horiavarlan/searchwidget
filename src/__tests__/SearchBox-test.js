import React from "react";
import { shallow, mount } from "enzyme";
import SearchBox from "../components/SearchBox";
import SearchWidget from "../components/SearchWidget";
import renderer from "react-test-renderer";
import SearchResults from "../components/SearchResults";

test("SearchBox has correct label", () => {
  const searchBox = shallow(<SearchBox />);
  const label = searchBox.find("#pickup_location_searchbox_label");
  expect(label.length).toEqual(1);
  expect(label.text()).toEqual("Pick-up location");
});

test("SearchBox has correct placeholder", () => {
  const searchBox = shallow(<SearchBox />);
  const input = searchBox.find("#pickup_location_searchbox");
  expect(input.props().placeholder).toEqual(
    "city, airport, station, region, district..."
  );
});

test("No results shown for one character", () => {
  const spy = jest.spyOn(SearchWidget.prototype, "getResults");
  const searchWidget = mount(<SearchWidget />);
  searchWidget.update();
  const searchBox = searchWidget.find("#pickup_location_searchbox");
  searchBox.simulate("change", { target: { value: "l" } });
  expect(spy).toNotHaveBeenCalled();
  serachBox.simulate("change", { target: { value: "london" } });
  expect(spy).toHaveBeenCalled();
});

test("No results message shown", () => {
  const searchResults = shallow(<SearchResults noResults={true} />);
  const searchResult = searchResults.find(".search-result");
  expect(searchResult.length).toEqual(1);
  expect(searchResult.props().className).toEqual(
    "search-result search-result--no-results"
  );
});
