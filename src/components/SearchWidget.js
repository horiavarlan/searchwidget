import React from "react";
import SearchResults from "./SearchResults";
import SearchBox from "./SearchBox";

const widgetStyle = {
  width: "100%",
  position: "relative"
};

export default class SearchWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false, noResults: false };
  }

  fetchData = (numberOfResults, searchTerm) => {
    if (searchTerm.length > 1) {
      this.setState({ isLoading: true });
      let fetchUrl = `${
        process.env.REACT_APP_API_URL
      }&solrRows=${numberOfResults}&solrTerm=${searchTerm}`;
      this.getResults(fetchUrl);
    } else {
      this.setState({ results: [], isLoading: false });
    }
  };

  getResults = fetchUrl => {
    fetch(fetchUrl)
      .then(response => {
        if (response.ok === true) {
          return response.json();
        }
      })
      .then(data => {
        if (data.results.numFound > 0) {
          let results = data.results.docs;
          this.setState({ results, isLoading: false, noResults: false });
        } else {
          this.setState({ results: [], isLoading: false, noResults: true });
        }
      })
      .catch(err => {
        this.setState({ results: [], isLoading: false, noResults: true });
      });
  };

  render() {
    return (
      <div style={widgetStyle}>
        <SearchBox
          fetchData={this.fetchData}
          isLoading={this.state.isLoading}
        />
        <SearchResults
          results={this.state.results}
          noResults={this.state.noResults}
        />
      </div>
    );
  }
}
