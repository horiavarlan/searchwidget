import React from "react";
import { debounce } from "lodash";
import PropTypes from "prop-types";

const labelStyle = {
  display: "block",
  fontSize: "0.875em",
  color: "#444",
  margin: "0 0 6px"
};

const inputStyle = {
  width: "100%",
  padding: "2% 8% 2% 2%",
  lineHeight: "15px",
  border: "2px solid #a9a9a9"
};

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = { search_text: "", typing: false, typingTimeout: 0 };
  }

  fetchData = debounce(e => {
    this.props.fetchData(6, this.state.search_text);
  }, 500);

  handleInput = event => {
    let element = event.target;
    this.setState({
      [element.name]: element.value
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.search_text !== this.state.search_text) {
      this.fetchData();
    }
  }

  render() {
    return (
      <div
        className={`search-box ${
          this.props.isLoading === true ? "search-box--loading" : ""
        }`}
      >
        <label
          htmlFor="pickup_location_searchbox"
          id="pickup_location_searchbox_label"
          style={labelStyle}
        >
          Pick-up location
        </label>
        <input
          type="text"
          name="search_text"
          id="pickup_location_searchbox"
          placeholder="city, airport, station, region, district..."
          value={this.state.search_text}
          onChange={this.handleInput}
          style={inputStyle}
          className="search-box-input"
        />
      </div>
    );
  }
}

SearchBox.propTypes = {
  isLoading: PropTypes.bool,
  fetchData: PropTypes.func
};

export default SearchBox;
