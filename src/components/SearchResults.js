import React from "react";
import SearchResult from "./SearchResult";
import PropTypes from "prop-types";

const SearchResults = props => {
  return (
    <div className="search-results__autocomplete">
      {props.results &&
        props.results.map(result => {
          const { city, country, placeType, region, name } = result;
          return (
            <SearchResult
              {...{ city, country, placeType, region, name }}
              key={result.bookingId}
            />
          );
        })}
      {props.noResults === true && (
        <div className="search-result search-result--no-results">
          No results found.
        </div>
      )}
    </div>
  );
};

SearchResult.propTypes = {
  results: PropTypes.array,
  noResults: PropTypes.bool
};

export default SearchResults;
