import React from "react";
import PropTypes from "prop-types";

const PlaceType = ({ placeType }) => {
  let name = "";
  let typeClass = "search-result__place-type ";
  switch (placeType) {
    case "A":
      name = "Airport";
      typeClass += "search-result__place-type--airport";
      break;
    case "C":
      name = "City";
      typeClass += "search-result__place-type--city";
      break;
    case "T":
      name = "Station";
      typeClass += "search-result__place-type--train-station";
      break;
    case "D":
      name = "District";
      typeClass += "search-result__place-type--district";
      break;
    case "W":
      name = "Region";
      typeClass += "search-result__place-type--region";
  }

  return <span className={typeClass}>{name}</span>;
};

PlaceType.propTypes = {
  placeType: PropTypes.string
};

const Region = ({ city, country, region }) => {
  let regionElements = [];
  if (typeof city !== "undefined" && city !== "") {
    regionElements.push(city);
  }
  if (typeof region !== "undefined" && region !== "") {
    regionElements.push(region);
  }
  if (typeof country !== "undefined" && country !== "") {
    regionElements.push(country);
  }

  let regionText = regionElements.join(", ");
  return <span className="search-result__location">{regionText}</span>;
};

Region.propTypes = {
  city: PropTypes.string,
  region: PropTypes.string,
  country: PropTypes.string
};

const SearchResult = props => {
  const { placeType, city, country, region } = props;
  return (
    <div className="search-result">
      <PlaceType placeType={props.placeType} />
      {props.name}
      <Region {...{ placeType, city, country, region }} />
    </div>
  );
};

SearchResult.propTypes = {
  city: PropTypes.string,
  country: PropTypes.string,
  name: PropTypes.string,
  placeType: PropTypes.string,
  region: PropTypes.string
};

export default SearchResult;
