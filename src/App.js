import React, { Component } from "react";
import "./App.css";
import "./css/Custom.css";
import SearchWidget from "./components/SearchWidget";

const formStyle = {
  background: "#F3CE56",
  borderBottom: "2px solid #DAB129",
  padding: "5%",
  width: "95%",
  margin: "0px auto",
  textAlign: "left"
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <div style={formStyle} className="main-search-form">
          <h1>Where are you going?</h1>
          <SearchWidget />
        </div>
      </div>
    );
  }
}

export default App;
